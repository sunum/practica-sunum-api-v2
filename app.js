'use strict';

const express = require('express');
const path = require('path');
const createError = require('http-errors');
const logger = require('morgan');
const JsonError = require('./src/utils/json-error');
const CustomError = require('./src/utils/custom-error');

const models = require('./src/api/models');
const app = express();

app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));

app.use(function (req, res, next) {
  res.header('Content-Type', 'application/json;charset=UTF-8');
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/public', express.static(path.join(__dirname, 'public')));

global.CustomError = CustomError;

app.use(JsonError);

require('./src/api')(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = err;
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;