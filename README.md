## Descripción 📌

API Http para práctica técnica SUNUM.

## Despliegue 📦

Instalar las librerías y dependencias del proyecto
_ npm install -g nodemon_

Iniciar el servidor http
_ nodemon bin/www _

## Construido con 🛠️

* [NodeJs](https://nodejs.org/es) - Entorno de ejecución del servidor Versión (^ v16.19.0)
* [SQLite](https://www.sqlite.org/index.html) - Base de datos
* [Sequelize](https://sequelize.org/) - ORM de base de datos

## Autores ✒️

_[Sunum.mx](https://sunum.mx/)_

* **Luis Mukul** - *Documentación* - [luis.mukul@sunum.mx]

## Licencia 📄

Este proyecto está bajo la Licencia (GNU AFFERO GENERAL PUBLIC LICENSE) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

---