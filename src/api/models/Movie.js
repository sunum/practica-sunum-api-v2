"use strict";

const { DataTypes, Model } = require("sequelize");

/**
 *
 * @param {Sequelize} sequelize
 * @returns {Model}
 */
const model = (sequelize) => {
  const Movie = sequelize.define(
    "Movie",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING(500),
        allowNull: true,
      },
      code: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: "movie",
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  Movie.associate = (models) => {
    // Movie.hasOne(models.ExampleModel,{
    //     foreignKey: 'id',
    // })
  };

  return Movie;
};

module.exports = model;
