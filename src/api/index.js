"use strict";

const express = require("express");
const router = express.Router();
const fs = require("fs");

const getFiles = () => {
  let files = [];

  fs.readdirSync(`${__dirname}/routes`).forEach((file) => files.push(file));

  return files;
};

const routesFiles = getFiles();

module.exports = (app) => {
  routesFiles.forEach((file) => {
    const { routes } = require(`${__dirname}/routes/${file}`);

    if (Array.isArray(routes)) {
      routes.forEach((route) => {
        const { method, path, action } = route;

        const newRoute = router[method](path, action);

        app.use("/api/", newRoute);
      });
    }
  });
};
