const { Movie } = require("../models");
const {} = require("express");

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns
 */

const createNewMovie = (req, res) => {
  return Promise.resolve()
    .then(async () => {
      const { name, description, code } = req.body;

      const movieModel = await Movie.create({ name, description, code });

      res.status(200).json({
        status: "OK",
        data: movieModel,
      });
    })
    .catch((error) => res.jsonError(error));
};

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns
 */

const getAllMovies = (req, res) => {
  return Promise.resolve()
    .then(async () => {
      const { name, code } = req.query;
      const criteria = {
        where: {},
      };

      if (name) {
        criteria.where.name = `%${name}%`;
      }

      if (code) {
        criteria.where.code = `${code}`;
      }

      const moviesList = await Movie.findAll(criteria);

      res.status(200).json({
        status: "OK",
        data: moviesList,
      });
    })
    .catch((error) => res.jsonError(error));
};

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
const getOneMovie = (req, res) => {
  return Promise.resolve()
    .then(async () => {
      const { id } = req.params;

      const movieModel = await Movie.findByPk(id);

      res.status(200).json({
        status: "OK",
        data: movieModel,
      });
    })
    .catch((error) => res.jsonError(error));
};

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns
 */

const deleteOneMovie = (req, res) => {
  return Promise.resolve()
    .then(async () => {
      const { id } = req.params;

      const movieModel = await Movie.findByPk(id);

      if (movieModel) {
        await movieModel.destroy();
      }

      res.status(200).json({
        status: "OK",
      });
    })
    .catch((error) => res.jsonError(error));
};

/**
 *
 * @param {Request} req
 * @param {Response} res
 * @returns
 */

const updateOneMovie = (req, res) => {
  return Promise.resolve()
    .then(async () => {
      const { id } = req.params;
      const { name, description, code } = req.body;

      const movieModel = await Movie.findByPk(id);

      if (movieModel) {
        await movieModel.update({
          name: name,
          description: description,
          code: code,
        });
      }

      res.status(200).json({
        status: "OK",
      });
    })
    .catch((error) => res.jsonError(error));
};

module.exports = {
  createNewMovie,
  getAllMovies,
  getOneMovie,
  deleteOneMovie,
  updateOneMovie,
};
