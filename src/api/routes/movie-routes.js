const {
  createNewMovie,
  getAllMovies,
  getOneMovie,
  deleteOneMovie,
  updateOneMovie,
} = require('../controllers/movie-controllers');

module.exports = {
  routes: [
    {
      method: 'post',
      path: '/movies',
      action: createNewMovie,
    },
    {
      method: 'get',
      path: '/movies',
      action: getAllMovies,
    },
    {
      method: 'get',
      path: '/movies/:id',
      action: getOneMovie,
    },
    {
      method: 'put',
      path: '/movies/:id',
      action: updateOneMovie,
    },
    {
      method: 'delete',
      path: '/movies/:id',
      action: deleteOneMovie,
    },
  ],
};
